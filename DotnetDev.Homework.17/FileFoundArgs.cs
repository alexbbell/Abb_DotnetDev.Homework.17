﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotnetDev.Homework._17
{
    public class FileFoundArgs : EventArgs
    {
        public string FoundFile { get; }
        
        public bool CancelRequested { get; set; }
        public event EventHandler<FileFoundArgs> FileFound;

        public FileFoundArgs(string fileName) => FoundFile = fileName;
    }
}
