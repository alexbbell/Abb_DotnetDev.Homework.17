﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DotnetDev.Homework._17
{

    //2.Написать класс, обходящий каталог файлов и выдающий событие при нахождении каждого файла;
    //3.Оформить событие и его аргументы с использованием .NET соглашений:
    //public event EventHandler FileFound;
    //FileArgs – будет содержать имя файла и наследоваться от EventArgs
    //4.	Добавить возможность отмены дальнейшего поиска из обработчика;
    //5.Вывести в консоль сообщения, возникающие при срабатывании событий и результат поиска максимального элемента.

    public class Filesearch
    {
        public event EventHandler<FileFoundArgs> FileFound;
        private System.Timers.Timer _timer;

        public void Search(string directory, string searchPattern)
        {

            _timer = new System.Timers.Timer(2000) { };
            _timer.Interval = 1000;
            

            foreach (var file in Directory.EnumerateFiles(directory, searchPattern))
            {
                var fileargs = RaiseFileFound(file);
                Console.WriteLine("file found");

                if (fileargs.CancelRequested) { break; }
            }
        }

        

        private FileFoundArgs RaiseFileFound(string file)
        {
            var args = new FileFoundArgs(file);
            FileFound?.Invoke(this, args);
            return args;
        }
            
    }
}
