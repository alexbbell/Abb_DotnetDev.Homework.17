﻿// See https://aka.ms/new-console-template for more information
using DotnetDev.Homework._17;


//Demo 
//1.Написать обобщённую функцию расширения, находящую и возвращающую максимальный элемент коллекции.
//Функция должна принимать на вход делегат, преобразующий входной тип в число для возможности поиска максимального значения.
//public static T GetMax(this IEnumerable e, Func<T, float> getParameter) where T : class;

ExtElement.Run();


//2.Написать класс, обходящий каталог файлов и выдающий событие при нахождении каждого файла;
//3.Оформить событие и его аргументы с использованием .NET соглашений:
//public event EventHandler FileFound;
//FileArgs – будет содержать имя файла и наследоваться от EventArgs
//4.	Добавить возможность отмены дальнейшего поиска из обработчика;

//5.Вывести в консоль сообщения, возникающие при срабатывании событий и результат поиска максимального элемента.

var fileLister = new Filesearch();
int filesFound = 0;


List<string> filenames = new List<string>();
EventHandler<FileFoundArgs> onFileFound = (sender, eventArgs) =>
{
    filenames.Add(eventArgs.FoundFile);
    eventArgs.CancelRequested = false;


    //4.	Добавить возможность отмены дальнейшего поиска из обработчика;
    //Для примера = отмена поиска, если найден файл .txt
    if (eventArgs.FoundFile.Contains(".txt")) eventArgs.CancelRequested = true;

    filesFound++;

};

fileLister.FileFound += onFileFound;

fileLister.Search(@"D:\Dest\", "*");

fileLister.FileFound -= onFileFound;

Console.WriteLine(String.Join(System.Environment.NewLine, filenames));
Console.WriteLine("Max length: " + filenames.Max(filenames => filenames.Length));
Console.WriteLine("filesFound: " + filesFound);

Console.WriteLine("---------");
