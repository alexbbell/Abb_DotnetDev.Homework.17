﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace DotnetDev.Homework._17
{

    //1.Написать обобщённую функцию расширения, находящую и возвращающую максимальный элемент коллекции.
    //Функция должна принимать на вход делегат, преобразующий входной тип в число для возможности поиска максимального значения.
    //public static T GetMax(this IEnumerable e, Func<T, float> getParameter) where T : class;

    public static class ExtElement 
    {
        interface IFloatWrapper
        {
            float Value { get; }
        }

        class FloatWrapper : IFloatWrapper
        {
            public float Value { get; }
            public FloatWrapper(float value)
            {
                Value = value;
            }
        }


        public static T GetMax<T>(this IEnumerable<T> collection, Func<T, float> getParameter) where T : class
        {
            T retVal = null;
            float max = float.MinValue;
            foreach (T item in collection)
            {
                float f = getParameter(item);
                if (f > max)
                {
                    retVal = item;
                    max = f;
                }                
            }
            return retVal;
        }


        //Подсмотрел синтаксис у коллег, решил применить
        private static float GetParameter<T>(T param) where T : class => param switch
        {
            IFloatWrapper wrapper => wrapper.Value,
            float fl => fl,
            _ => throw new ArgumentException("Unable to convert type")
        };



        public static void Run()
        {
            List<IFloatWrapper> list = new () { new FloatWrapper(1f), new FloatWrapper(4.55f), new FloatWrapper(-1f) };
            float retVal = list.GetMax<IFloatWrapper>(GetParameter).Value;
            Console.WriteLine(retVal);
        }

    }
}
